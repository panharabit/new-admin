Rails.application.routes.draw do
  resources :variants
    # resources :sessions

    namespace :admin do

      root "dashboards#index"

      devise_scope :user do
        get "/sign_in" => "devise/sessions#new" # custom path to login/sign_in
        get "/sign_up" => "devise/registrations#new", as: "new_user_registration" # custom path to sign_up/registration
      end

      devise_for :users, controllers: { sessions: 'admin/users/sessions'}, :skip => [:registrations]
        as :user do
          get 'users/edit' => 'devise/registrations#edit', :as => 'edit_user_registration'
          put 'users' => 'devise/registrations#update', :as => 'user_registration'
          patch 'users' => 'devise/registrations#update'
      end

      resources :users
      resources :categories
      resources :products do
        resources :stock_items
        get "stock" => "stock_items#stock", :as => "stock"
      end
      resources :stock_locations

      get "dashboard" => "dashboards#index"
      # get "imports" => "imports#index", :as=>"imports"
      # post "imports/" => "imports#create"
      # get "imports/new" => "imports#new", :as=>"new_import"
      # get "imports/:id/edit" => "imports#edit", :as=>"edit_import"
      # get "imports/:id" => "imports#show", :as=>"import"
      # patch "imports/:id" => "imports#update"
      # put "imports/:id" => "imports#update"
      # delete "imports/:id" => "imports#destroy"
    end

    root "homes#index"
    scope :shopping do
        resources :homes
        resources :products
        get "customer_info/:id" => "customers#customer_info", :as => :customer_info
        get "customer_info/:id/edit_customer_info" => "customers#edit_customer_info", :as => "edit_customer_info"
        patch "customer_info/:id" => "customers#update_customer_info"
        put "customer_info/:id" => "customers#update_customer_info"
        get "product_detail/:id" => "products#product_detail", :as => :product_detail
        resources :customers
        get "cart" => "cart#show"
        get "cart/add/:id" => "cart#add", :as => :add_to_cart
        post "cart/remove/:id" => "cart#remove", :as => :remove_from_cart
        post "cart/checkout" => "cart#checkout", :as => :checkout

        post 'session' => 'sessions#create', :as=> :session_customer
        get 'signup' => 'customers#new',:as=> :signup
        get 'logout' => 'sessions#destroy', :as => :logout
        get 'login' => 'sessions#new', :as => :login
      end

  end
