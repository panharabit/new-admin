class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :product_name
      t.string :sku
      t.decimal :master_price
      t.decimal :cost_price
      t.date :available_on
      t.date :discount_on
      t.text :description
      t.text :meta_description
      t.string :meta_keyword
      t.string :color
      t.boolean :promotionable
      t.integer :category_id

      t.timestamps
    end
  end
end
