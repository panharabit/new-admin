class CreateStockLocations < ActiveRecord::Migration[5.1]
  def change
    create_table :stock_locations do |t|
      t.string :name
      t.string :street
      t.string :city
      t.string :zip
      t.string :phone
      t.string :country
      t.string :state
      t.boolean :active
      t.integer :user_id

      t.timestamps
    end
  end
end
