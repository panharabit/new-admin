class AddStockItemIdToProducts < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :stock_item_id, :integer
  end
end
