class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  #Callback

  enum role: [:user, :vip, :admin]
  after_initialize :set_default_role, :if => :new_record?
  after_initialize :set_role_vip, :if => :new_record?
  # Association
  has_many :categories
  has_many :products
  has_many :stock_items
  has_many :stock_locations
  #Devise
  devise :database_authenticatable,:registerable,
  :recoverable, :rememberable, :trackable, :validatable

  #Validation
  has_attached_file :image, style: { medium: "300x500>",thumb: "100x100>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/


  #instance method
  def set_default_role
    self.role ||= :user
  end

  def set_role_vip
    self.role ||= :vip
  end

  def full_name
    "#{first_name}" "#{last_name}"
  end
end
