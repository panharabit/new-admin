class Customer < ApplicationRecord
  #Association
has_many :orders, :dependent => :destroy
validates :password, presence: true
attr_accessor :password
before_save :prepare_password

#Validation
has_attached_file :image, styles: {
  medium: "300x300>",
  thumb: "100x100>"
}
validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
validates_presence_of :username
validates_uniqueness_of :username, :email, :allow_blank => true
validates_format_of :username, :with => /(?=.*[A-Za-z._@])/, :allow_blank => true, :message => "should only contain letters, numbers, or .-_@"
validates_format_of :email, :with => /\A[-a-z0-9_+\.]+\@([-a-z0-9]+\.)+[a-z0-9]{2,4}\z/i
validates_presence_of :password, :on => :create
validates_confirmation_of :password
validates_length_of :password, :minimum => 4, :allow_blank => true

#Class Method
def self.authenticate(login, pass)
  customer = find_by_username(login) || find_by_email(login)
  return customer if customer && customer.matching_password?(pass)
end

#Instance Method
def matching_password?(pass)
  self.password_hashL == encrypt_password(pass)
end

private

  def prepare_password
    unless password.blank?
      self.password_salt = Digest::SHA1.hexdigest([Time.now, rand].join)
      self.password_hashL = encrypt_password(password)
    end
  end

  def encrypt_password(pass)
    Digest::SHA1.hexdigest([pass, password_salt].join)
  end
end
