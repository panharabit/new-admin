class StockItem < ApplicationRecord
  belongs_to :user

  belongs_to :stock_location
  belongs_to :product

  #instance method

  #Validation
  validates :quantity, numericality: { only_integer: true, :greater_than_or_equal_to => 1 }

end
