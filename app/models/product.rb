class Product < ApplicationRecord
  before_save :update_slug
  #Call Back
  #Association
  belongs_to :category
  belongs_to :user

  has_many :stock_items
  has_many :stock_location, :through => :stock_items

  #Validation
  validates :product_name, presence: true
  #instance method
  def update_slug
    self.slug = product_name.parameterize
  end

  def to_param
    slug
  end

  def count_on_hand?
    count = stock_items.sum(:quantity)
    if count == 0
      "Out Stock"
    else
      return count
    end
  end



end
