class Category < ApplicationRecord
  #CallBack
  before_save :update_slug

  #Association
  belongs_to :user
  has_many :products

  #Validation
  validates :category_name, presence: true

  #instance method
  def update_slug
    self.slug = category_name.parameterize
  end

  def to_param
    slug
  end
end
