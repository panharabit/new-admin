class ApplicationController < ActionController::Base
  layout 'application'
  protect_from_forgery
  before_action :configure_permitted_parameters, if: :devise_controller?
  protected
    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:first_name,:last_name,:role,:username,:email,:password,:password_confirmation,:remember_me) }
      devise_parameter_sanitizer.permit(:sign_in) { |u| u.permit(:login,:first_name,:last_name,:role,:username,:email,:password,:remember_me)}
      devise_parameter_sanitizer.permit(:account_update) { |u| u.permit(:first_name,:last_name,:role,:username,:email,:dateofbirth,:current_password,:password,:password_confirmation) }
    end

    def after_sign_out_path_for(resource_or_scope)
      admin_user_session_path
    end

    def after_sign_in_path_for(resource_or_scope)
      admin_dashboard_path
    end


end
