class NewApplicationController < ActionController::Base
  layout 'new_application'
  include Authentication
  protect_from_forgery
end
