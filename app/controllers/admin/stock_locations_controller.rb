class Admin::StockLocationsController < ApplicationController
  before_action :set_stock_location, only: [:edit, :update, :destroy]
  before_action :authenticate_admin_user! , only: [:index,:new,:edit,:update,:destroy]
  before_action :admin_only, only: [:new,:edit,:update,:create,:destroy]
  # GET /stock_locations
  # GET /stock_locations.json
  def index
    @stock_locations = StockLocation.all
    @fields = ["ID","Name"]
  end

  # GET /stock_locations/1
  # GET /stock_locations/1.json
  def show
    @stock_location = StockLocation.find(params[:id])
    @stock_items = @stock_location.stock_items    
    # @stock_items = StockItem.all.joins(:product).group(:product_name).order(id: :asc)
    @fields = ["Product","Quantity"]
  end

  # GET /stock_locations/new
  def new
    @stock_location = StockLocation.new
  end

  # GET /stock_locations/1/edit
  def edit
  end

  # POST /stock_locations
  # POST /stock_locations.json
  def create
    @stock_location = StockLocation.new(stock_location_params)

    respond_to do |format|
      if @stock_location.save
        format.html { redirect_to admin_stock_location_path(@stock_location), notice: 'Stock location was successfully created.' }
        format.json { render :show, status: :created, location: @stock_location }
      else
        format.html { render :new }
        format.json { render json: @stock_location.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /stock_locations/1
  # PATCH/PUT /stock_locations/1.json
  def update
    respond_to do |format|
      if @stock_location.update(stock_location_params)
        format.html { redirect_to admin_stock_location_path(@stock_location), notice: 'Stock location was successfully updated.' }
        format.json { render :show, status: :ok, location: @stock_location }
      else
        format.html { render :edit }
        format.json { render json: @stock_location.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stock_locations/1
  # DELETE /stock_locations/1.json
  def destroy
    @stock_location.destroy
    respond_to do |format|
      format.html { redirect_to admin_stock_locations_url, notice: 'Stock location was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_stock_location
      @stock_location = StockLocation.find(params[:id])
    end

    def admin_only
      unless current_admin_user.admin?
        redirect_to admin_dashboard_path, :alert => "Access denied."
      end
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def stock_location_params
      params.require(:stock_location).permit(:name, :street, :city, :zip, :phone, :country, :state, :active)
    end
end
