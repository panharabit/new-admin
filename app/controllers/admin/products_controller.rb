class Admin::ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_admin_user!, only: [:index,:new,:edit,:update,:destroy]
  before_action :admin_only, only: [:index,:new,:edit,:update,:create,:destroy]
  # GET /categories
  # GET /categories.json
  def index
    @products = Product.all
    @fields = ["ID","Product","Price","Category","User"]
  end

  # GET /categories/1
  # GET /categories/1.json
  def show
  end

  # GET /categories/new
  def new
    @product = current_admin_user.products.build
  end

  # GET /categories/1/edit
  def edit
  end

  # POST /categories
  # POST /categories.json
  def create
    @product = current_admin_user.products.build(create_product_params)

    respond_to do |format|
      if @product.save
        format.html { redirect_to edit_admin_product_path(@product), notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /categories/1
  # PATCH/PUT /categories/1.json
  def update
    respond_to do |format|
      if @product.update(update_product_params)
        format.html { redirect_to edit_admin_product_path(@product), notice: 'Category was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to admin_products_url, notice: 'Category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find_by_slug(params[:id])
    end

    def admin_only
      unless current_admin_user.admin?
        redirect_to admin_dashboard_path, :alert => "Access denied."
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def create_product_params
      params.require(:product).permit(:product_name,:sku,:master_price,:category_id)
    end
    def update_product_params
      params.require(:product).permit(:product_name,:sku,:master_price,:category_id,
                                      :cost_price,:available_on,:discount_on,:description,
                                      :slug,:meta_description,:meta_keyword,:color,:category_id,
                                      :promotionable)
    end
end
