json.extract! category, :id, :product_name,:product_price, :user_id, :created_at, :updated_at
json.url product_url(product, format: :json)
