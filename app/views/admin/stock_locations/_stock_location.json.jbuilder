json.extract! stock_location, :id, :name, :street, :city, :zip, :phone, :country, :state, :active, :created_at, :updated_at
json.url stock_location_url(stock_location, format: :json)
